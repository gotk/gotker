package main

import (
	"fmt"
	"io/fs"
	"io/ioutil"
	"os"
	ose "os/exec"
	"strings"
	"time"

	"gitlab.com/gotk/gotker/toolkit"
	"gopkg.in/yaml.v3"
)

type GotkConfig struct {
	Commit           string `yaml:"commit"`              // 当前commit
	UpdatedAt        string `yaml:"updated-at"`          // 更新时间
	UpdatedAtUnix    int64  `yaml:"updated-at-unix"`     // 更新时间戳 小于1小时 则不检测更新
	CheckedAtUnix    int64  `yaml:"checked-at-unix"`     // 检测时间戳 检测间隔小于15分钟 则不检测更新
	Message          string `yaml:"message"`             // 更新信息
	NewCommit        string `yaml:"new-commit"`          // 仓库最新版本
	NewUpdatedAt     string `yaml:"new-updated-at"`      // 仓库最新代码提交时间
	NewUpdatedAtUnix int64  `yaml:"new-updated-at-unix"` // 时间戳
	NewMessage       string `yaml:"new-message"`         // 仓库最新提交信息
	UserID           int64  `yaml:"userid"`              // git 用户ID
	Username         string `yaml:"username"`            // git 用户名
	Email            string `yaml:"email"`               // git 邮箱
}

func getGotkConfigFilePATH() (string, error) {
	// 检测环境
	var homeDir string
	if toolkit.GetGOOS() == toolkit.Windows {
		homeDir = os.Getenv("USERPROFILE")
		if homeDir == "" {
			driveName := os.Getenv("HOMEDRIVE")
			homePath := os.Getenv("HOMEPATH")
			homeDir = fmt.Sprintf("%s%s\\", driveName, homePath)
		} else {
			homeDir = fmt.Sprintf("%s\\", homeDir)
		}
	} else {
		homeDir = os.Getenv("HOME")
		if homeDir == "" {
			userData, err := ose.Command("sh", "-c", "eval echo $USER").CombinedOutput()
			if err != nil {
				_, _ = fmt.Fprintf(os.Stderr, "get user info err: %+v\n", err)
				return "", err
			}
			userName := strings.ReplaceAll(string(userData), "\n", "")
			if userName == "" {
				_, _ = fmt.Fprintf(os.Stderr, "get username empty")
				return "", fmt.Errorf("get username empty")
			}
			if userName == "root" {
				homeDir = "/root/"
			} else {
				homeDir = fmt.Sprintf("/home/%s/", userName)
			}
		} else {
			homeDir = fmt.Sprintf("%s/", homeDir)
		}
	}
	configFile := fmt.Sprintf("%s%s", homeDir, gotkConfig)
	return configFile, nil
}

// checkCliVersion 判别是否有新版本
func checkCliVersion() bool {
	configFile, err := getGotkConfigFilePATH()
	if err != nil {
		return false
	}
	var config = GotkConfig{}

	_, err = os.Stat(configFile)
	if err != nil {
		if !os.IsNotExist(err) {
			_, _ = fmt.Fprintf(os.Stderr, "check gotker config err: %+v\n", err)
			return false
		}
		// 新建配置文件
		data, err := yaml.Marshal(config)
		if err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "marshal config file err: %+v\n", err)
			return false
		}
		if err := ioutil.WriteFile(configFile, data, fs.ModePerm); err != nil {
			_, _ = fmt.Fprintf(os.Stderr, "write new gotker config err: %+v\n", err)
			return false
		}
	}
	content, err := ioutil.ReadFile(configFile)
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "open gotker config err: %+v\n", err)
		return false
	}

	if err := yaml.Unmarshal(content, &config); err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "unmarshal gotker config err: %+v\n", err)
		return false
	}

	// 限制检测频率 1小时
	var tnu = time.Now().Unix()
	if tnu-config.UpdatedAtUnix < updateCheckInterval {
		return false
	}
	// 检测时间间隔
	if tnu-config.CheckedAtUnix < checkInterval {
		return false
	}

	// 获取仓库最新提交
	commit, err := toolkit.GetRepoLatestCommit()
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "get repo commit info err: %+v\n", err)
		return false
	}

	// 比较是否要更新
	if config.Commit == commit.ShortID {
		config.CheckedAtUnix = tnu
		_ = rewriteConfig(configFile, config)
		return false
	}

	if config.Commit == "" {
		config.Commit = "-"
	}
	if config.UpdatedAt == "" {
		config.UpdatedAt = "-"
	}
	ca := commit.CommittedDate
	_, _ = fmt.Fprintf(os.Stdout, "gotker有新版本: %s || 更新时间: %s\n", commit.ShortID, ca.Format("2006-01-02 15:04:05"))
	_, _ = fmt.Fprintf(os.Stdout, "当前gotker版本: %s || 更新时间: %s\n", config.Commit, config.UpdatedAt)
	_, _ = fmt.Fprintf(os.Stdout, "你可以执行 gotker update 进行手动更新\n")

	config.NewCommit = commit.ShortID
	config.NewMessage = toolkit.Trim(commit.Message)
	config.NewUpdatedAt = ca.Format("2006-01-02 15:04:05")
	config.NewUpdatedAtUnix = ca.Unix()
	config.CheckedAtUnix = ca.Unix()

	_ = rewriteConfig(configFile, config)

	return true
}

func rewriteConfig(path string, config GotkConfig) error {
	newBody, err := yaml.Marshal(config)
	if err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "marshal gotker config err: %+v\n", err)
		return err
	}

	if err := ioutil.WriteFile(path, newBody, fs.ModePerm); err != nil {
		_, _ = fmt.Fprintf(os.Stderr, "rewrite gotker config err: %+v\n", err)
		return err
	}
	return nil
}
