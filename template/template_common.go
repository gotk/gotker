package template

const templateBody = `package common

import (
	"encoding/json"
	"io"
	"io/ioutil"
	"reflect"
	"unsafe"
)

func Read(ioRead io.ReadCloser, s interface{}) (err error) {
	bytes, err := ioutil.ReadAll(ioRead)
	if err != nil {
		return err
	}
	return json.Unmarshal(bytes, s)
}

func ReadAndValidate(ioRead io.ReadCloser, requestParams RequestParams) (err error) {
	bytes, err := ioutil.ReadAll(ioRead)
	if err != nil {
		return err
	}
	if err := json.Unmarshal(bytes, requestParams); err != nil {
		return err
	}
	// 校验参数
	return requestParams.Validate()
}

// zero-copy
func bytes2string(b []byte) string{
	sliceHeader := (*reflect.SliceHeader)(unsafe.Pointer(&b))
	sh := reflect.StringHeader{
		Data: sliceHeader.Data,
		Len: sliceHeader.Len,
	}
	return *(*string)(unsafe.Pointer(&sh))
}

type RequestParams interface {
	Validate() error
}
 `

const templateCode = `package common

const (
	//参数出错
	CodeParamError = 40000
	//服务错误
	CodeServiceError = 40010
	//下游出错
	CodeBelowError = 40020

	//sso没有登录或者过期
	NoLoginError = 50005
)
`

const templateLog = `package common

import "gitlab.heywoods.cn/go-sdk/omega/component/log"

var Logger *log.Logger

`

const templateResponse = `package common

import "reflect"

const SuccessErrCode = 0

const SuccessErrMsg = "success"

type Result struct {
	ErrCode int      ` + "`" + `json:"err_code"` + "`" + `
	ErrMsg  string    ` + "`" + `json:"err_msg"` + "`" + `
	Data    interface{} ` + "`" + `json:"data"` + "`" + `
}

func SuccessResponse(data interface{}) string {
	// 为了兼容前端
	dataV := reflect.ValueOf(data)
	if dataV.Kind() == reflect.Ptr || dataV.Kind() == reflect.Slice {
		if dataV.IsNil() {
			data = []string{}
		}
	} else if data == nil {
		data = []string{}
	}
	r := Result{SuccessErrCode, SuccessErrMsg, data}
	bytes, _ := json.Marshal(r)
	bodyStr := string(bytes)
	return bodyStr
}

func FailResponse(code int, msg string) string {
	r := Result{code, msg, nil}
	bytes, _ := json.Marshal(r)
	bodyStr := string(bytes)
	return bodyStr
}
`

const TemplateRandom = `package common

import (
	"crypto/hmac"
	"crypto/md5"
	"crypto/sha256"
	"encoding/hex"
	"fmt"
	"io"
	"math/rand"
	"time"
)

func RandInt64(min, max int64) int64 {
	if min >= max {
		return max
	}
	return rand.Int63n(max-min) + min
}

func RandomString() string {
	return Md5(uuid.TimeUUID().String())
}

func RandomNumberString() string {
	rand.NewSource(int64(time.Nanosecond))
	f := 10000 + rand.Intn(89999)
	var now = time.Now().UnixNano() / 1e6

	return fmt.Sprintf("%d%d", now, f)
}

func Md5(str string) string {
	m := md5.New()
	_, _ = io.WriteString(m, str)
	return hex.EncodeToString(m.Sum(nil))
}

func HmacSha256(secret string, str string) string {
	h := hmac.New(sha256.New, []byte(secret))
	_, _ = h.Write([]byte(str))
	return hex.EncodeToString(h.Sum(nil))
}`
