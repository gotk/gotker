package template

const templateProto = `syntax = "proto3";

package model;

option go_package = "{{.Name}}/model";
`
