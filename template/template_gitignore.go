package template

const templateGitignore = `.idea
go.sum
{{.Name}}
{{.Name}}.exe
{{.Name}}.exe~
config_local.yaml
`
