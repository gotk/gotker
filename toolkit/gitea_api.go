package toolkit

import (
	"fmt"
	"time"

	"github.com/guonaihong/gout"
)

const TimeOut = time.Second * 5

var defaultHeader = gout.H{
	"Accept":     "application/json",
	"User-Agent": "GOTK/beta",
}

type RepoCommits []*RepoCommit

type RepoCommit struct {
	ID             string    `json:"id"`
	ShortID        string    `json:"short_id"`
	Title          string    `json:"title"`
	AuthorName     string    `json:"author_name"`
	AuthorEmail    string    `json:"author_email"`
	AuthoredDate   time.Time `json:"authored_date,omitempty"`
	CommitterName  string    `json:"committer_name"`
	CommitterEmail string    `json:"committer_email"`
	CommittedDate  time.Time `json:"committed_date,omitempty"`
	CreatedAt      time.Time `json:"created_at"`
	Message        string    `json:"message"`
	ParentIds      []string  `json:"parent_ids"`
	WebURL         string    `json:"web_url"`
}

// GetRepoLatestCommit 获取仓库最新提交记录
func GetRepoLatestCommit() (*RepoCommit, error) {
	var commits RepoCommits

	api := "https://gitlab.com/api/v4/projects/33974436/repository/commits"

	if err := gout.GET(api).SetHeader(defaultHeader).SetQuery(gout.H{
		"page":  1,
		"limit": 5,
	}).SetTimeout(TimeOut).BindJSON(&commits).Do(); err != nil {
		return nil, err
	}

	if len(commits) == 0 {
		return nil, fmt.Errorf("empty commit")
	}
	return commits[0], nil
}
