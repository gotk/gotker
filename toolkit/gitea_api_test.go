package toolkit

import (
	"fmt"
	"testing"
)

func TestGetRepoLatestCommit(t *testing.T) {
	res, err := GetRepoLatestCommit()
	if err != nil {
		panic(err)
	}

	fmt.Println(res)
}
