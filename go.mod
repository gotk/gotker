module gitlab.com/gotk/gotker

go 1.16

require (
	github.com/elliotchance/pie v1.39.0
	github.com/go-playground/validator/v10 v10.10.0 // indirect
	github.com/guonaihong/gout v0.2.11
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.3.0
	gitlab.com/gotk/toolkit v0.0.0-20220223081633-6b1774a9b954
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292 // indirect
	gopkg.in/yaml.v3 v3.0.0-20210107192922-496545a6307b
)
